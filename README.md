# Cowboys vs Dragons
https://bitbucket.org/jamesajohnson/code-challenge

## Teams

``` team 1
Chris
Rory
Jade
```

``` team 2
Scott T
Rohan
```

``` team 3
Scott P
Luke
```
## Things To Add

* Player can move in all directions,
* Add a goal to screen,
* The dragon chases the player,
* If the dragon catched the player, the player loses.
* If the player gets to the goal, the player wins,
* Score Counter,
* Donut World/ Map Wrap,
* Difficulty Picker,
* Character Picker