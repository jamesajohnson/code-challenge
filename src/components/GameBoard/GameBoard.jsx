import React, { Component } from 'react';

import InputHandler from '../../InputHandler';
import BoardTile from '../BoardTile/BoardTile';

const inputHandler = new InputHandler();

class GameBoard extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            board: this.emptyBoard(),
            player: {
                x: 13,
                y: 13,
                sprite: '🤠'
            },
            goal: {
                x: 2,
                y: 2,
                sprite: '💰'
            },
            dragon: {
                x: 13,
                y: 6,
                sprite: '🐉'
            }
        };

        this.addTilesToBoard();
        this.defineInputs();
    }

    // Add new Characters to the Board Here
    addTilesToBoard() {
        this.resetBoard();
        this.addPlayerToBoard();
    }

    // Empties the board
    resetBoard() {
        this.setState({
            board: this.emptyBoard()
        });
    }

    addPlayerToBoard() {
        const x = this.state.player.x;
        const y = this.state.player.y;

        let newBoard = this.state.board;
        newBoard[y][x] = this.state.player.sprite;
        
        this.setState({
            board: newBoard
        });
    }

    renderGameBoard() {
        const rows = this.state.board.map((row, rowIndex) =>
            <div key={rowIndex} className='game-board__row'> {
                row.map((cell, cellIndex) => 
                    <BoardTile key={`${rowIndex}${cellIndex}`} sprite={cell} />
                )
            } </div>
        );

        return rows;
    }

    // Called by InputHandler on Each Key Press
    // Use this to move the player
    defineInputs() {
        inputHandler.onKeyLeft(() => {
            console.log('Left Key Pressed');

            const newPlayer = this.state.player;
            newPlayer.x --;

            this.setState({
                player: newPlayer
            });

            this.addTilesToBoard();
        });

        inputHandler.onKeyUp(() => {
            console.log('Up Key Pressed');
        });

        inputHandler.onKeyRight(() => {
            console.log('Right Key Pressed');
        });

        inputHandler.onKeyDown(() => {
            console.log('Down Key Pressed');
        });
    }

    emptyBoard ()  {
        return [
            ['','','','','','','','','','','','','','','',],
            ['','','','','','','','','','','','','','','',],
            ['','','','','','','','','','','','','','','',],
            ['','','','','','','','','','','','','','','',],
            ['','','','','','','','','','','','','','','',],
            ['','','','','','','','','','','','','','','',],
            ['','','','','','','','','','','','','','','',],
            ['','','','','','','','','','','','','','','',],
            ['','','','','','','','','','','','','','','',],
            ['','','','','','','','','','','','','','','',],
            ['','','','','','','','','','','','','','','',],
            ['','','','','','','','','','','','','','','',],
            ['','','','','','','','','','','','','','','',],
            ['','','','','','','','','','','','','','','',],
            ['','','','','','','','','','','','','','','',]
        ];
    }

    render() {
        return (
            <div className='game-board'>
                { this.renderGameBoard() }
            </div>
        );
    }
}

export default GameBoard;