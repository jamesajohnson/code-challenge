import React, { Component } from 'react';

class BoardTile extends React.Component {

    render() {
        return (
            <div className='board-tile'>
                { this.props.sprite }
            </div>
        );
    }
}

export default BoardTile;