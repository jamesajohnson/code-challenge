class InputHandler {

    onKeyLeft(callback) {
        window.addEventListener('keyup', (e) => {
            if (e.key === 'ArrowLeft') {
                callback();
            }
        });
    }

    onKeyUp(callback) {
        window.addEventListener('keyup', (e) => {
            if (e.key === 'ArrowUp') {
                callback();
            }
        });
    }

    onKeyRight(callback) {
        window.addEventListener('keyup', (e) => {
            if (e.key === 'ArrowRight') {
                callback();
            }
        });
    }

    onKeyDown(callback) {
        window.addEventListener('keyup', (e) => {
            if (e.key === 'ArrowDown') {
                callback();
            }
        });
    }
}

export default InputHandler;