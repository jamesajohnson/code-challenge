import React, { Component } from 'react';

import Hello from './components/Hello/Hello';
import GameBoard from './components/GameBoard/GameBoard';

class App extends React.Component {

    render() {
        return (
            <div>
                <Hello />
                <GameBoard />
            </div>
        );
    }
}

export default App;